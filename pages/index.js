import Head from 'next/head';
import Image from 'next/image';
import { Inter } from '@next/font/google';
import styles from '@/styles/Home.module.css';
import { useEffect, useState } from 'react';
import { headers } from 'next/dist/client/components/headers';

const inter = Inter({ subsets: ['latin'] });

export default function Home() {
  const [todo, setTodo] = useState([]);
  const [addTodo, setAddTodo] = useState([]);
  useEffect(() => {
    async function fetchData() {
      const res = await fetch('/api/todos');
      const data = await res.json();
      setTodo(data);
    }
    fetchData();
  }, []);

  const clickHandler = async () => {
    const res = fetch('/api/todos', {
      method: 'POST',
      body: JSON.stringify({ todo: addTodo }),
      headers: { 'Content-Type': 'application/json' },
    });
    const data = await (await res).json();
    console.log(data);
  };

  // const deleteHandler = async () => {
  //   const res = fetch('/api/todos', {
  //     method: 'DELETE',
  //     // headers: {
  //     //   'Content-Type': 'application/json',
  //     // },
  //   });
  //   const data = (await res).json();
  //   //setTodo(data.data);
  //   console.log('step1');
  //   console.log(data);
  //   console.log('steo2');
  // };
  async function removeItems() {
    // const response = await fetch('/api/todos', {
    //   method: 'DELETE',
    // });
    // const data = await response.json();
    // console.log(data);
    console.log('data');
  }
  return (
    <div className={styles.main}>
      <h2>Hello Next Js API</h2>
      {todo.map(item => (
        <li key={item.id}>{item.title}</li>
      ))}
      <div>
        <input value={addTodo} onChange={e => setAddTodo(e.target.value)} />
        <button style={{ padding: '1rem' }} onClick={clickHandler}>
          Create Todo
        </button>
      </div>
      <div>
        <button style={{ padding: '1rem' }} onClick={removeItems}>
          Delete
        </button>
      </div>
    </div>
  );
}
